<?php

declare(strict_types=1);

// namespace DoctrineMigrations; // For dev

namespace App\Migrations; // For test/prod

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * New emails.
 */
final class Version20231107145600 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.)');
        $this->addSql("UPDATE `paragraph` SET `text` = '<h2>Répondre aux enjeux de la transition écologique</h2>\r\n<div style=\"text-align:center;\"><img src=\"/images/pages/more/LOGOS_GAL_D_et_DP.jpg\" alt=\"\" /></div>\r\n<p>En 2020, Provence Alpes Agglomération s’est dotée d’un Plan Climat Air Energie Territorial et a intégré le dispositif\r\n    « Territoire Engagé pour la Transition<br>Ecologique » de l’ADEME. Ainsi, conformément aux objectifs inscrits dans\r\n    la loi sur la transition énergétique, d’ici 2030, le territoire s’est engagé à :<br></p>\r\n<ul>\r\n    <li>\r\n        <p>réduire de 40 % ses émissions de gaz à effet de serre (par rapport à 1990) ;</p>\r\n    </li>\r\n    <li>\r\n        <p>réduire de 20 % ses consommations d’énergie (par rapport à 2012).<br></p>\r\n    </li>\r\n</ul>\r\n<p>Dans cette optique, Provence Alpes Agglomération a souhaité élaborer un outil de covoiturage.<br>Soutenu par le\r\n    programme européen LEADER et la Région SUD-PACA (GAL LEADER DURANCE PROVENCE, GAL LEADER DIGNOIS), ce dispositif\r\n    permet d’organiser et de favoriser la mise en relation entre passagers et conducteurs afin d’optimiser les\r\n    déplacements du quotidien à l’échelle du territoire.\r\n</p>\r\n<div style=\"text-align:center;\"><img src=\"/images/pages/more/Plan_de_financement_PAA.jpg\" alt=\"\" /></div>' WHERE `paragraph`.`id` = 99;");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.)');
    }
}
